//
//  NotesModel.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 22/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import Foundation
import Combine
import PostalApi
import os.log

class NotesViewModel: ObservableObject {
    @Published var notes: [Note] = []
    @Published var error: String = ""
    let service:PostalApi
    
    private var cancellableSet: Set<AnyCancellable> = []
    @Published var searchText: String = "" {
        didSet {
            if oldValue != self.searchText {
                $searchText
                    .removeDuplicates()
                    .throttle(for: 2,
                              scheduler: RunLoop.current,
                              latest: true)
                    .sink { (text) in
                        self.fetch()
                }
                .store(in: &cancellableSet)
            }
        }
    }

    init(with service: Postal) {
        self.service = service
        self.error = ""
    }

    deinit {
        cancellableSet.removeAll()
    }

    /// Fetch notes
    func fetch(){
        service.getNotes()
            .throttle(for: 1,
                      scheduler: RunLoop.current,
                      latest: true)
            .sink(receiveCompletion: { completion in
                    switch(completion) {
                    case .finished:
                        self.error = ""
                    case.failure(let error): os_log("%@", error.localizedDescription)
                        self.error = error.localizedDescription
                    }
                	self.cancellableSet.removeAll()
                  },
                  receiveValue: { allNotes in
                    DispatchQueue.main.async { [weak self] in
                        if let text = self?.searchText, text.count > 0 {
                            self?.notes = allNotes.filter{
                                $0.title
                                    .lowercased()
                                    .contains(text.lowercased())
                            }
                        }
                        else {
                            self?.notes = allNotes
                        }
                    }

            }).store(in: &cancellableSet)
    }

    /// Delete note
    /// - Parameter indexSet: index of a note marked for deletion
    func delete(at indexSet:IndexSet){
        guard let index = indexSet.first else {
            return
        }
        let note = notes[index]
        service.deleteNote(id: String(note.id))
            .sink(receiveCompletion: { completion in
                switch(completion) {
                case .finished: break
                case.failure(let error): os_log("%@", error.localizedDescription)
                }
                self.cancellableSet.removeAll()
            },
                  receiveValue: { result in
                    DispatchQueue.main.async { [weak self] in
                        if result == 204 {
                            self?.notes.remove(at: index)
                        }
                    }
            }).store(in: &cancellableSet)
    }

    /// Manage notes
    /// - Parameters:
    ///   - id: identifier of the note to update - if not set or empty then new note will be created
    ///   - noteText: note text
    func manageNote(id: String?, noteText: String) {
        //One might consider moving the decision on if Create or Update is to be used down to the PostalApi
        //however for demonstration purposes left it here on the app level to decide
        let request = NoteRequest(title: noteText)
        if let id = id, id.count > 0 {
            service.putNote(id: id, note: request)
                .sink(receiveCompletion: { completion in
                    switch(completion) {
                    case .finished: break
                    case.failure(let error): os_log("%@", error.localizedDescription)
                    }
                    self.cancellableSet.removeAll()
                },
                      receiveValue: { result in
                        DispatchQueue.main.async { [weak self] in
                            self?.notes.removeAll(where: { (note) -> Bool in
                                String(note.id) == id
                            })
                            self?.notes.append(result)
                        }
                }).store(in: &cancellableSet)
        }
        else {
            service.postNote(request)
                .sink(receiveCompletion: { completion in
                    switch(completion) {
                    case .finished: break
                    case.failure(let error): os_log("%@", error.localizedDescription)
                    }
                },
                      receiveValue: { result in
                        DispatchQueue.main.async { [weak self] in
                            self?.notes.append(result)
                        }
                }).store(in: &cancellableSet)

        }
    }
}
