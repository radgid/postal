//
//  NotesViewModelMock.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 24/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import Foundation
import Combine
import PostalApi

/// Mock of the NotesViewModel
class NotesViewModelMock: ObservableObject {
    //Ah, would be simple if EnvironmentObject in SwiftUI could be mocked - but the View insists on having the exact type NotesViewModel and not the mock
//    @Published var notes: [Note] = [Note(id: 1, title: "Short note text"),
//                                    Note(id: 2, title: "Long note text to display possibly through two lines"),
//                                    Note(id: 3, title: "Long note text to display possibly through three lines \nand with new line character and unicode 😃")]

        lazy var environmentObject: NotesViewModel = {
            let service = Postal()
            let model = NotesViewModel(with: service)
            model.notes = [Note(id: 1, title: "Short note text"),
                           Note(id: 2, title: "Long note text to display possibly through two lines"),
                           Note(id: 3, title: "Long note text to display possibly through three lines \nand with new line character and unicode 😃")]

            return model
        }()

    func mockNetworking(
        data: Data = .init(),
        response: URLResponse = .init()
    ) -> Networking {
        return { _ in
            Just((data: data, response: response))
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        }
    }
}
