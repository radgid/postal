//
//  ContentView.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 22/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import SwiftUI
import PostalApi


enum NotesState {
    case list
    case manage
}

struct ContentView: View {
    @EnvironmentObject var notesModel:NotesViewModel
    @State private var isManageNote: Bool = false
    @State private var angle: Double = 0
    @State private var selectedNote: Note?

    //MARK: View Definition
    var body: some View {
        NavigationView{
            VStack(alignment: .center, spacing: 10){
                List{
                    TextField("Search", text: $notesModel.searchText)
                        .textFieldStyle(RoundedBorderTextFieldStyle())
                    GeometryReader { geometry -> ActivityIndicator? in
                        let frame = geometry.frame(in: CoordinateSpace.global)
                        if frame.origin.y > 200 {
                            self.notesModel.fetch()
                            return
                                ActivityIndicator(isAnimating: .constant(true),
                                    indicatorSize: 20)

                        }
                        else{
                            return nil
                        }
                    }
                    if self.notesModel.notes.count == 0 {
                        HStack{
                            Spacer()
                            Text("No notes found")
                                .foregroundColor(.gray)
                            Spacer()
                        }
                    }
                    ForEach(notesModel.notes){ note in
                        Button(action: {
                            self.selectedNote = note
                            self.isManageNote = !self.isManageNote
                        },
                               label: {
                                NoteRow(note: note)
                        })
                    }
                    .onDelete(perform: self.deleteItem)
                }
                .modifier(NoteListModifier())

                Spacer()
                Button(action: {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                        self.selectedNote = nil
                        self.isManageNote = !self.isManageNote
                    }
                    self.angle = 0
                    withAnimation(Animation.easeInOut(duration: 0.3)) { self.angle = 360 }
                }, label: {
                    Text("+")
                        .padding([.bottom],4)
                        .modifier(ViewMaximized())
                        .modifier(ShadowModifier())
                        .rotationEffect(Angle(degrees: angle))
                    }).modifier(CircleButtonModifier())

            }.navigationBarTitle("Notes")
        }
        .sheet(isPresented: $isManageNote, content:{
            self.manageNoteView()
        })
        .onAppear{
            self.notesModel.fetch()
        }

    }


    //MARK: - Methods
    private func deleteItem(at indexSet: IndexSet){
        notesModel.delete(at: indexSet)
    }

    private func manageNoteView() -> some View {
        var noteId: String? = nil
        if self.selectedNote?.id != nil {
            noteId = String(self.selectedNote!.id)
        }
        return ManageNote(noteText: self.selectedNote?.title ?? "" ,
                   id: noteId,
                   isManageNote: self.$isManageNote)
            .environmentObject(self.notesModel)
    }
}
    //MARK: - Preview
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let content = ContentView().environmentObject(NotesViewModelMock().environmentObject)
        return content
    }
}


