//
//  Color+Extension.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 24/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import Foundation
import SwiftUI

extension Color {

    static func backgroundColor(for colorScheme: ColorScheme) -> Color {
        if colorScheme == .dark {
            return Color.init(red: 0.1, green: 0.1, blue: 0.1)
        } else {
            return Color.white
        }
    }

    static func linearGradient() -> LinearGradient {
        return LinearGradient(gradient: Gradient(colors: [Color.accentColor,.white]),
                              startPoint: .bottom,
                              endPoint: .top)
    }

    static func radialGradient() -> RadialGradient {
        return RadialGradient(gradient: Gradient(colors: [.white,.accentColor]),
                              center: .center,
                              startRadius: 0,
                              endRadius: 40)

    }

}
