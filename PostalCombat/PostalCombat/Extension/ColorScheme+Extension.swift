//
//  ColorScheme+Extension.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 24/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import SwiftUI

extension ColorScheme {
    func next() -> ColorScheme {
        switch (self){
        case .dark: return .light
        case .light: return .dark
        @unknown default: return .light
        }
    }
}
