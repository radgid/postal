//
//  UITextView+Extension.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 26/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import UIKit

extension UITextView {
    @objc func onTapDone() {
        self.resignFirstResponder()
    }
}
