//
//  ListModifier.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 24/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import Foundation
import SwiftUI

struct NoteCellModifier: ViewModifier {
    @Environment(\.colorScheme) var colorScheme: ColorScheme

    func body(content: Content) -> some View {
        let backgroundColorPerScheme = Color.backgroundColor(for: self.colorScheme)
        let shadowColorPerInvertedScheme = Color.backgroundColor(for: self.colorScheme.next()).opacity(0.3)

        return content
            .frame( minHeight: 80, maxHeight: 80)
            .background(backgroundColorPerScheme)
            .cornerRadius(4)
            .shadow(color: shadowColorPerInvertedScheme, radius: 2, x: 0, y: 0)
    }
}

