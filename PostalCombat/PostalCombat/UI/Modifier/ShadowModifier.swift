//
//  ShadowModifier.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 25/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import SwiftUI

struct ShadowModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .shadow(color: Color.black, radius: 1, x: 0, y: -1)
        }
}
