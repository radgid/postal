//
//  NoteListModifier.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 24/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import Foundation
import SwiftUI

struct NoteListModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .onAppear(){
                UITableView.appearance().separatorStyle = .none
        }
    }
}
