//
//  ButtonModifier.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 24/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import SwiftUI

struct ButtonModifier: ViewModifier {
	@Environment(\.colorScheme) var colorScheme: ColorScheme

    func body(content: Content) -> some View {

        let shadowColorPerInvertedScheme = Color.backgroundColor(for: self.colorScheme.next()).opacity(0.7)

    	return  content
        .frame(
            maxWidth: .infinity,
            maxHeight: 60,
            alignment: .center
        )
        .background(Color.accentColor.opacity(0.8))
        .foregroundColor(Color.white)
        .cornerRadius(6)
        .padding(EdgeInsets(top: 5, leading: 10, bottom: 10, trailing: 10))
        .shadow(color: shadowColorPerInvertedScheme, radius: 3, x: 0, y: 0)
    }

}

struct CircleButtonModifier: ViewModifier {
    @Environment(\.colorScheme) var colorScheme: ColorScheme

    func body(content: Content) -> some View {

        let shadowColorPerInvertedScheme = Color.backgroundColor(for: self.colorScheme.next()).opacity(0.7)

        return  content
        .frame(
            maxWidth: 60,
            maxHeight: 60,
            alignment: .center
        )
        .background(Color.accentColor)
        .foregroundColor(Color.white)
        .cornerRadius(30)
        .font(.system(size: 40))
        .padding(EdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10))
        .shadow(color: shadowColorPerInvertedScheme, radius: 2, x: 0, y: 0)
    }

}
