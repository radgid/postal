//
//  ViewMaximizedModifier.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 24/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import SwiftUI

struct ViewMaximized: ViewModifier {

    let maxHeight: CGFloat
    let alignment: Alignment

    init(maxHeight: CGFloat = .infinity, alignment: Alignment = .center) {
        self.maxHeight = maxHeight
        self.alignment = alignment
    }

    func body(content: Content) -> some View {
        content
            .frame(
                maxWidth: .infinity,
                maxHeight: maxHeight,
                alignment: alignment
            )
    }

}
