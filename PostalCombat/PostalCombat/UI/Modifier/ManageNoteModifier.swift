//
//  ManageNoteModifier.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 24/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import SwiftUI

struct ManageNoteModifier: ViewModifier {
    @Environment(\.colorScheme) var colorScheme: ColorScheme

    func body(content: Content) -> some View {
        let backgroundColorPerScheme = Color.backgroundColor(for: self.colorScheme)
        let shadowColorPerInvertedScheme = Color.backgroundColor(for: self.colorScheme.next()).opacity(0.3)

        return content
            .background(backgroundColorPerScheme)
            .cornerRadius(4)
            .padding([.leading,.trailing],10)
            .frame( minHeight: 350, maxHeight: 350, alignment: .top)
            .shadow(color: shadowColorPerInvertedScheme, radius: 2, x: 0, y: 0)
    }
}
