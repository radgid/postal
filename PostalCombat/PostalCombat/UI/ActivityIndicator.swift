//
//  ActivityIndicator.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 27/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import SwiftUI

struct ActivityIndicator: View {
    @Binding var isAnimating: Bool
    @State private var angle: Double = 0
    @Environment(\.colorScheme) private var colorScheme: ColorScheme
    let indicatorSize: CGFloat

    private var foreverAnimation: Animation {
        Animation.easeInOut(duration: 0.5)
            .repeatForever(autoreverses: false)
    }

    var body: some View {
        return
            HStack{
                Spacer()
                Text("📝")
                    .shadow(color: .black, radius: 2, x: 1, y: 1)
                    .font(.system(size: indicatorSize))
                    .frame(alignment: .center)
                    .rotationEffect(Angle(degrees: isAnimating ? angle : 0))
                    .onAppear() {
                        withAnimation(self.foreverAnimation) { self.angle = 360 }
                }
                Spacer()
        }
    }
}

struct ActivityIndicator_Previews: PreviewProvider {
    static var previews: some View {
        return ActivityIndicator(isAnimating: .constant(true), indicatorSize: 20)
    }
}


