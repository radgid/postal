//
//  ActivityIndicator.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 26/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import SwiftUI

struct ActivityIndicatorOverlay: View {
    @Binding var isAnimating: Bool
    @State private var angle: Double = 0
    @Environment(\.colorScheme) private var colorScheme: ColorScheme

    private var foreverAnimation: Animation {
        Animation.easeOut(duration: 0.5)
            .repeatForever(autoreverses: false)
    }

    var body: some View {

        let shadowColorPerInvertedScheme = Color.backgroundColor(for: self.colorScheme.next()).opacity(0.4)

        return
            VStack{
                VStack{
                    ActivityIndicator(isAnimating: $isAnimating, indicatorSize: 30)
                }.frame(width: 100, height: 100, alignment: .center)
                    .background(Color.blue.opacity(0.2))
                    .cornerRadius(10)
                    .shadow(color: shadowColorPerInvertedScheme, radius: 2, x: 0, y: 0)
            }
             .modifier(ViewMaximized())
             .background(Color.black.opacity(0.1))
        	 .edgesIgnoringSafeArea(.all)
    }
}

struct ActivityIndicatorOverlay_Previews: PreviewProvider {
    static var previews: some View {
        return ActivityIndicatorOverlay(isAnimating: .constant(true))
    }
}

