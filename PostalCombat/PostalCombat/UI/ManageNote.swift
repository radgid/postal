//
//  ManageNote.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 24/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//

import SwiftUI
import PostalApi

/** Manage note - create or update the note
    - creates new one  if no id is passed
    - updates the currently selected note based on the id **/
struct ManageNote: View {
    @State var noteText: String
    let id: String?
    @EnvironmentObject var notesModel: NotesViewModel
    @Binding var isManageNote: Bool

    @State private var loading: Bool = false

    //MARK: - View definition
    var body: some View {
        ZStack{
            VStack{
                Text("Manage Note")
                    .font(Font.system(size: 24))
                    .padding()
                Spacer()
                VStack{
                    SimpleTextView(text: $noteText)
                        .modifier(ManageNoteModifier())
                    Spacer()
                    Button(action: {
                        self.save()
                    }, label: {
                        Text("Save")
                            .font(Font.system(size: 25))
                        	.modifier(ViewMaximized())
                            .modifier(ShadowModifier())
                    }).modifier(ButtonModifier())
                }.padding()
            }
            if self.loading{
                ActivityIndicatorOverlay(isAnimating: $loading)
            }
        }.onReceive(notesModel.objectWillChange, perform: {
            self.loading = false
            self.isManageNote = false
        })
    }

    //MARK: - Methods
    private func save() {
        loading = true
        notesModel.manageNote(id: id, noteText: noteText)
    }
}

struct ManageNote_Previews: PreviewProvider {
    static var previews: some View {
        return ManageNote(noteText: "Longer text in here for the note\nspanning multiple lines. ",
                          id: nil,
                          isManageNote: .constant(true)).environmentObject(NotesViewModelMock().environmentObject)
    }
}
