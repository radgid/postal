//
//  NoteRow.swift
//  PostalCombat
//
//  Created by Danko, Radoslav on 22/01/2020.
//  Copyright © 2020 Danko, Radoslav. All rights reserved.
//
import SwiftUI
import PostalApi

struct NoteRow: View {
    let note: Note
    var body: some View {
        HStack{
            Text(note.title)
                .modifier(ViewMaximized())
        }
        .modifier(NoteCellModifier())
    }
}

struct NoteRow_Previews: PreviewProvider {
    static var previews: some View {
        return NoteRow(note: NotesViewModelMock().environmentObject.notes[0])
    }
}
