# Postal

Simple iOS project demonstrating Apple Combine and SwiftUI

## Prerequisites
The project requires:

* iOS 13
* Mac OS Catalina

The project is using [Apple Combine]("https://developer.apple.com/documentation/combine") for asynchronous networking and accessing values over time and [SwiftUI](https://developer.apple.com/xcode/swiftui/) for the user interface 

## Installation
Just git clone the repo from bitbucket
```
git clone https://bitbucket.org/radgid/postal.git
```
## Project Structure
The project is split into two directories

* PostalApi - Swift Package containing the simple client for [Notes Mock API]("https://note10.docs.apiary.io/#")
* PostalCombat - XCode project of simple app for Notes management

### Dependencies

The PostalCombat XCode project includes PostalApi package as a local package dependency.
It is possible to put the PostalApi into the standalone repository for easier inclusion into further project by using Swift Package Manager.

Refer to [Adding Package Dependencies]("https://developer.apple.com/documentation/xcode/adding_package_dependencies_to_your_app for details") for details.

### Limitations
* UI is targetting iPhones primarily and was not enhanced for use on tablets yet

Works OK on iPhones, iPad and OSX.