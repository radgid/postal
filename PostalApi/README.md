# PostalApi

This is the Swift package for Notes API Mock present at 
 http://private-9aad-note10.apiary- mock.com/
 
## Installation
Include this package as a dependency to your app

Refer to [Adding Package Dependencies]("https://developer.apple.com/documentation/xcode/adding_package_dependencies_to_your_app for details") for details.

### Testing
There two Test suites available:
* Mock
* Integration tests

Both can be run from the terminal:

```
swift test
```

Or simply run them from the Test Navigator
