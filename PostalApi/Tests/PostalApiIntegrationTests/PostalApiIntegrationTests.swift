import XCTest
import os.log
import Combine
@testable import PostalApi

final class PostalApiIntegrationTests: XCTestCase {

    func testGetNotes() {
        let exp = expectation(description: "Get Notes")

        let cancellable =
            Postal().getNotes()
                .sink(receiveCompletion: { completion in
                    switch(completion){
                    case .finished: break
                    case .failure(let error):
                        os_log("error:%@", error.localizedDescription)
                        XCTFail("Failed to get notes")
                    }
                    exp.fulfill()
                }
                    , receiveValue: { notes in
                        os_log("notes:%@", notes)
                })

        XCTAssertNotNil(cancellable)
        wait(for: [exp], timeout: 10)

    }

    func testGetNote() {
        let exp = expectation(description: "Get Note")

        let cancellable =
            Postal().getNote(id: "123")
                .sink(receiveCompletion: { completion in
                    switch(completion){
                    case .finished: break
                    case .failure(let error):
                        os_log("error:%@", error.localizedDescription)
                        XCTFail("Failed to get note")
                    }
                    exp.fulfill()
                }
                    , receiveValue: { note in
                        os_log("note:%@", note.description)
                })

        XCTAssertNotNil(cancellable)
        wait(for: [exp], timeout: 10)

    }

    func testPostNote() {
        let exp = expectation(description: "Post Note")

        let noteRequest = NoteRequest(title: "My test note")
        let cancellable =
            Postal().postNote(noteRequest)
                .sink(receiveCompletion: { completion in
                    switch(completion){
                    case .finished: break
                    case .failure(let error):
                        os_log("error:%@", error.localizedDescription)
                        XCTFail("Failed to post note")
                    }
                    exp.fulfill()
                }
                    , receiveValue: { note in
                        //One would assert if noteRequest.title == note.title
                        os_log("note:%@", note.description)
                })



        XCTAssertNotNil(cancellable)
        wait(for: [exp], timeout: 10)

    }

    func testPutNote() {
        let exp = expectation(description: "Put Note")

        let noteRequest = NoteRequest(title: "My updated test note")
        let cancellable =
            Postal().putNote(id: "1", note: noteRequest)
                .sink(receiveCompletion: { completion in
                    switch(completion){
                    case .finished: break
                    case .failure(let error):
                        os_log("error:%@", error.localizedDescription)
                        XCTFail("Failed to update note")
                    }
                    exp.fulfill()
                }
                    , receiveValue: { note in
                        //One would assert if noteRequest.title == note.title
                        os_log("note:%@", note.description)
                })

        XCTAssertNotNil(cancellable)
        wait(for: [exp], timeout: 10)

    }

    func testDeleteNote() {
        let exp = expectation(description: "Delete Note")

        let cancellable =
            Postal().deleteNote(id: "123")
                .sink(receiveCompletion: { completion in
                    switch(completion){
                    case .finished: break
                    case .failure(let error):
                        os_log("error:%@", error.localizedDescription)
                        XCTFail("Failed to delete note")
                    }
                    exp.fulfill()
                }
                    , receiveValue: { statusCode in
                        XCTAssert(statusCode == 204)
                        os_log("statusCode:%d", statusCode)
                })



        XCTAssertNotNil(cancellable)
        wait(for: [exp], timeout: 10)

    }


    static var allTests = [
        ("testGetNotes", testGetNotes,
         "testGetNote", testGetNote,
         "testPostNote", testPostNote,
         "testPutNote", testPutNote,
         "testDeleteNote", testDeleteNote),
    ]
}
