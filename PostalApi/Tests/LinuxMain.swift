import XCTest

import PostalApiTests

var tests = [XCTestCaseEntry]()
tests += PostalApiTests.allTests()
XCTMain(tests)
