import XCTest
import os.log
import Combine
@testable import PostalApi

struct NotesMock {
    static var allNotes: String = #"""
        [
          {
            "id": 1,
            "title": "MOCK Jogging in park"
          },
          {
            "id": 2,
            "title": "MOCK Pick-up posters from post-office"
          }
        ]
"""#
    static var note: String = #"""
{
  "id": 2,
  "title": "MOCK Pick-up posters from post-office"
}
"""#
}

final class PostalApiTests: XCTestCase {

    func mockNetworking(
        data: Data = .init(),
        response: URLResponse = .init()
    ) -> Networking {
        return { _ in
            Just((data: data, response: response))
                .setFailureType(to: Error.self)
                .eraseToAnyPublisher()
        }
    }

    func testGetNotesMock() {
        let exp = expectation(description: "Get Notes Mock")

        let mockData = NotesMock.allNotes.data(using: .utf8) ?? Data()
        let cancellable =
            Postal(networking: mockNetworking(data: mockData)).getNotes()
                .sink(receiveCompletion: { completion in
                    switch(completion){
                    case .finished: break
                    case .failure(let error):
                        os_log("error:%@", error.localizedDescription)
                        XCTFail("Failed to get notes")
                    }
                    exp.fulfill()
                }
                    , receiveValue: { notes in
                        os_log("notes:%@", notes)
                })

        XCTAssertNotNil(cancellable)
        wait(for: [exp], timeout: 10)

    }

    func testGetNoteMock() {
        let exp = expectation(description: "Get Note Mock")

        let mockData = NotesMock.note.data(using: .utf8) ?? Data()
        let cancellable =
            Postal(networking: mockNetworking(data: mockData)).getNote(id: "123")
                .sink(receiveCompletion: { completion in
                    switch(completion){
                    case .finished: break
                    case .failure(let error):
                        os_log("error:%@", error.localizedDescription)
                        XCTFail("Failed to get note")
                    }
                    exp.fulfill()
                }
                    , receiveValue: { note in
                        os_log("note:%@", note.description)
                })

        XCTAssertNotNil(cancellable)
        wait(for: [exp], timeout: 10)

    }
    func testPostNoteMock() {
        let exp = expectation(description: "Post Note Mock")

        let mockData = NotesMock.note.data(using: .utf8) ?? Data()
        let noteRequest = NoteRequest(title: "My test note")
        let cancellable =
            Postal(networking: mockNetworking(data: mockData)).postNote(noteRequest)
                .sink(receiveCompletion: { completion in
                    switch(completion){
                    case .finished: break
                    case .failure(let error):
                        os_log("error:%@", error.localizedDescription)
                        XCTFail("Failed to post note")
                    }
                    exp.fulfill()
                }
                    , receiveValue: { note in
                        //One would assert if noteRequest.title == note.title
                        os_log("note:%@", note.description)
                })

        XCTAssertNotNil(cancellable)
        wait(for: [exp], timeout: 10)

    }

    func testPutNoteMock() {
        let exp = expectation(description: "Put Note Mock")

        let mockData = NotesMock.note.data(using: .utf8) ?? Data()
        let noteRequest = NoteRequest(title: "My updated test note")
        let cancellable =
            Postal(networking: mockNetworking(data: mockData)).putNote(id: "1", note: noteRequest)
                .sink(receiveCompletion: { completion in
                    switch(completion){
                    case .finished: break
                    case .failure(let error):
                        os_log("error:%@", error.localizedDescription)
                        XCTFail("Failed to update note")
                    }
                    exp.fulfill()
                }
                    , receiveValue: { note in
                        //One would assert if noteRequest.title == note.title
                        os_log("note:%@", note.description)
                })

        XCTAssertNotNil(cancellable)
        wait(for: [exp], timeout: 10)

    }

    func testDeleteNoteMock() {
        let exp = expectation(description: "Delete Note Mock")

        let mockData = NotesMock.note.data(using: .utf8) ?? Data()
        let mockUrlResponse = HTTPURLResponse(url: URL(string:"http://mock")!,
                                              statusCode: 204,
                                              httpVersion: "1.0",
                                              headerFields: nil)!

        let cancellable =
            Postal(networking: mockNetworking(data:mockData,
                                              response: mockUrlResponse)).deleteNote(id: "123")
                .sink(receiveCompletion: { completion in
                    switch(completion){
                    case .finished: break
                    case .failure(let error):
                        os_log("error:%@", error.localizedDescription)
                        XCTFail("Failed to delete note")
                    }
                    exp.fulfill()
                }
                    , receiveValue: { statusCode in
                        XCTAssert(statusCode == 204)
                        os_log("statusCode:%d", statusCode)
                })

        XCTAssertNotNil(cancellable)
        wait(for: [exp], timeout: 10)

    }

    static var allTests = [
        ("testGetNotesMock", testGetNotesMock,
         "testGetNoteMock", testGetNoteMock,
         "testPostNoteMock", testPostNoteMock,
         "testPutNoteMock", testPutNoteMock,
         "testDeleteNoteMock", testDeleteNoteMock),
    ]
}
