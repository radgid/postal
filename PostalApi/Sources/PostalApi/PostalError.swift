//
//  File.swift
//  
//
//  Created by Danko, Radoslav on 22/01/2020.
//

import Foundation

public enum PostalError: Error {
    case invalidUrl
    case encodingError
    case reason(error: Error)
    case serverError(code: Int)
}

