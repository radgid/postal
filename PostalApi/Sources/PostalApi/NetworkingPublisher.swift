//
//  File.swift
//  
//
//  Created by Danko, Radoslav on 27/01/2020.
//

import Foundation
import Combine

public typealias NetworkingPublisher = AnyPublisher<(data: Data, response: URLResponse), Error>
public typealias Networking = (URLRequest) -> AnyPublisher<(data: Data, response: URLResponse), Error>

extension NetworkingPublisher {
    func unwrap<T>() -> AnyPublisher<T, Error> where T: Decodable {
        return self.tryMap{ (data: Data, response: URLResponse) -> Data in
            return data
        }
        .decode(type: T.self, decoder: JSONDecoder()).eraseToAnyPublisher()
    }
}
