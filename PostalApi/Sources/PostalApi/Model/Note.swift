//
//  File.swift
//  
//
//  Created by Danko, Radoslav on 22/01/2020.
//

import Foundation

/// Note structure
public struct Note: Codable, Identifiable, Hashable {
    public let id: Int
    public let title: String

    public init(id: Int, title: String) {
        self.id = id
        self.title = title
    }
}

public struct NoteRequest: Codable {
    public let title: String

    public init(title: String) {
        self.title = title
    }
}

extension Note: CustomStringConvertible {

    public var description: String {
        return "{ id: \(id), title: \(title) }"
    }

}
