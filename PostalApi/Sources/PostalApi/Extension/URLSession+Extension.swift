//
//  File.swift
//  
//
//  Created by Danko, Radoslav on 27/01/2020.
//

import Foundation
import Combine

extension URLSession {
    func erasedDataTaskPublisher(
        for request: URLRequest
    ) -> NetworkingPublisher {
        dataTaskPublisher(for: request)
            .mapError { $0 }
            .eraseToAnyPublisher()
    }
}
