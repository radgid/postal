import Foundation
import Combine
import os.log

/// Postal API - Public endpoints
public protocol PostalApi{
    func getNotes() -> AnyPublisher<[Note],PostalError>
    func getNote(id: String) -> AnyPublisher<Note,PostalError>
    func postNote(_ note:NoteRequest) -> AnyPublisher<Note, PostalError>
    func putNote(id: String, note:NoteRequest) -> AnyPublisher<Note, PostalError>
    func deleteNote(id: String) -> AnyPublisher<Int,PostalError>
}

/// Postal API - Notes client
public struct Postal: PostalApi {

    let networking: Networking
    
    public init(){
        self.networking = URLSession.shared.erasedDataTaskPublisher
    }

    init(networking: @escaping Networking) {
        self.networking = networking
    }

    private let hostUrl = URL(string: "http://private-9aad-note10.apiary-mock.com/")

    /// Retrieve notes from server
    public func getNotes() -> AnyPublisher<[Note], PostalError> {
        guard let url = URL(string: "notes", relativeTo: hostUrl) else {
            return Fail(error: .invalidUrl).eraseToAnyPublisher()
        }

        let request = URLRequest(url: url)
        let publisher:AnyPublisher<[Note], PostalError> = networking(request)
        	.unwrap()
            .mapError { error -> PostalError in
                .reason(error: error)
            }
            .eraseToAnyPublisher()

        return publisher
    }

    /// Retrieve specified note
    /// - Parameter id: identifier of the note to retrieve
    public func getNote(id: String) -> AnyPublisher<Note, PostalError> {
        guard let url = URL(string: "notes/\(id)", relativeTo: hostUrl) else {
            return Fail(error: .invalidUrl).eraseToAnyPublisher()
        }

        let request = URLRequest(url: url)
        let publisher:AnyPublisher<Note, PostalError> = networking(request)
            .unwrap()
            .mapError { error -> PostalError in
                .reason(error: error)
            }
            .eraseToAnyPublisher()

        return publisher
    }

    public func postNote(_ note:NoteRequest) -> AnyPublisher<Note, PostalError> {
        guard let url = URL(string: "notes", relativeTo: hostUrl) else {
            return Fail(error: .invalidUrl).eraseToAnyPublisher()
        }
        guard let noteRequest = try? JSONEncoder().encode(note) else {
            return Fail(error: .encodingError).eraseToAnyPublisher()
        }

        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = noteRequest
        let publisher:AnyPublisher<Note, PostalError> = networking(request)
            .unwrap()
            .mapError { error -> PostalError in
                .reason(error: error)
            }
            .eraseToAnyPublisher()

        return publisher
    }

    public func putNote(id: String, note:NoteRequest) -> AnyPublisher<Note, PostalError> {
        guard let url = URL(string: "notes/\(id)", relativeTo: hostUrl) else {
            return Fail(error: .invalidUrl).eraseToAnyPublisher()
        }
        guard let noteRequest = try? JSONEncoder().encode(note) else {
            return Fail(error: .encodingError).eraseToAnyPublisher()
        }

        var request = URLRequest(url: url)
        request.httpMethod = "PUT"
        request.httpBody = noteRequest
        let publisher:AnyPublisher<Note, PostalError> = networking(request)
            .unwrap()
            .mapError { error -> PostalError in
                .reason(error: error)
            }
            .eraseToAnyPublisher()

        return publisher
    }

    public func deleteNote(id: String) -> AnyPublisher<Int,PostalError> {
        guard let url = URL(string: "notes/\(id)", relativeTo: hostUrl) else {
            return Fail(error: .invalidUrl).eraseToAnyPublisher()
        }

        var request = URLRequest(url: url)
        request.httpMethod = "DELETE"
        let publisher = networking(request)
            .tryMap { (data: Data, response: URLResponse) -> Int in
                guard let urlResponse = response as? HTTPURLResponse else {
                    return -1
                }
                return urlResponse.statusCode
        	}
            .mapError { error -> PostalError in
                .reason(error: error)
        	}
        	.eraseToAnyPublisher()

        return publisher
    }
}
